"""
Provide function to pull all the dependencies of this project.
"""

load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

def repositories():
    """Download repositories for build """

    _unicodedata2_version = "14.0.0"

    http_archive(
        name = "com_github_fonttools_unicodedata2",
        sha256 = "f95b778ae3c2706c4dfab731261c0c60aa2bd93b9c0ba9de49395b0c45b12a6a",
        build_file = "//third_party:unicodedata2.BUILD",
        strip_prefix = "unicodedata2-{0}".format(_unicodedata2_version),
        urls = [
            "https://github.com/fonttools/unicodedata2/archive/refs/tags/{0}.tar.gz".format(_unicodedata2_version),
        ],
    )

    _pybind11_bazel_commit = "72cbbf1fbc830e487e3012862b7b720001b70672"

    http_archive(
        name = "pybind11_bazel",
        sha256 = "fec6281e4109115c5157ca720b8fe20c8f655f773172290b03f57353c11869c2",
        strip_prefix = "pybind11_bazel-{0}".format(_pybind11_bazel_commit),
        urls = [
            "https://github.com/pybind/pybind11_bazel/archive/{0}.zip".format(_pybind11_bazel_commit),
        ],
    )

    _pybind11_version = "2.9.2"

    http_archive(
        name = "pybind11",
        build_file = "@pybind11_bazel//:pybind11.BUILD",
        sha256 = "6bd528c4dbe2276635dc787b6b1f2e5316cf6b49ee3e150264e455a0d68d19c1",
        strip_prefix = "pybind11-{0}".format(_pybind11_version),
        urls = [
            "https://github.com/pybind/pybind11/archive/v{0}.tar.gz".format(_pybind11_version),
        ],
    )
