workspace(name = "hermetic-python")

load("//tools:repositories.bzl", "repositories")

repositories()

load("@//tools:python_interpreter.bzl", "py_download")

_PYTHON_VERSION = "3.8.13"

py_download(
    name = "py_darwin_x86_64",
    arch = "x86_64",
    os = "darwin",
    sha256 = "d8e816bea432ae2859de6aeb6914831705914ac8fabaed9fd08ebff8c4975e54",
    urls = [
        "https://github.com/indygreg/python-build-standalone/releases/download/20220528/cpython-{0}+20220528-x86_64-apple-darwin-install_only.tar.gz".format(_PYTHON_VERSION),
    ],
)

py_download(
    name = "py_darwin_aarch64",
    arch = "aarch64",
    os = "darwin",
    sha256 = "bc1914f76eb6465ae8abaae0958d4276d1514f936ec5767005dbcda76c2d968f",
    urls = [
        "https://github.com/indygreg/python-build-standalone/releases/download/20220528/cpython-{0}+20220528-aarch64-apple-darwin-install_only.tar.gz".format(_PYTHON_VERSION),
    ],
)

py_download(
    name = "py_linux_x86_64",
    arch = "x86_64",
    os = "linux",
    sha256 = "8c97b4d2305be6b76362265ea8449fc071c387b26bfd2a09ca8a7879543fa7dc",
    urls = [
        "https://github.com/indygreg/python-build-standalone/releases/download/20220528/cpython-{0}+20220528-x86_64-unknown-linux-gnu-install_only.tar.gz".format(_PYTHON_VERSION),
    ],
)

# Register these toolchains
register_toolchains(
    "@py_darwin_x86_64//:toolchain",
    "@py_darwin_aarch64//:toolchain",
    "@py_linux_x86_64//:toolchain",
)

load("@pybind11_bazel//:python_configure.bzl", "python_configure")

python_configure(
    name = "local_config_python",
    python_interpreter_target = "@py_linux_x86_64//:bin/python3",
)
